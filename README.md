# Zweck des Programms

Dies ist ein Programm zur Konvertierung der Kontoauszüge von [Bitcoins.de](https://www.bitcoin.de) und [Fidor.de](https://www.fidor.de/) in ein für [Portfolio Performance](https://www.portfolio-performance.info/) passendes Format. Das Ergebnis soll dann direkt importiert werden können und bildet die Cryptowährungsumsätze in PP ab.

# Datenbeschaffung

## In Bitcoin.de

* Gehe auf "Übersicht / Kontoauszug"
* Wähle als "Datum" den vollständigen Bereich der verfügbaren Umsätze aus
* Setze den Typ auf "Alles"
* Klicke den Link "Kontoauszug als CSV inkl. nachrichtlicher Anzeige der Fidor-Gebühr herunterladen" an

## In Fidor.de

... to be done ...

## Kursverlauf

* [Bitcoin](https://www.ariva.de/btc-eur_bitcoin_-_euro-kurs/historische_kurse)
