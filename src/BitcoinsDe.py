'''
Created on 03.04.2020

@author: Chris
'''

from csv import DictReader


class BitcoinsDe(object):
    '''
    :param csvfile: CSV file to read
    :type csvfile: str or file
    '''

    def __init__(self, csvfile):
        self._csvfile = csvfile

        self._read_csvfile(self._csvfile)

    def _read_csvfile(self, filename):
        with open(filename, encoding='utf-8') as csvfile:
            content = DictReader(csvfile, delimiter=';')

            for row in content:
                if row['Typ'] in ['Kauf', 'Verkauf', 'Einzahlung', 'Auszahlung', 'Netzwerk-Gebühr', ]:
                    print(row['Datum'], row['Währung'], row['Kurs'],
                          row['BTC nach Bitcoin.de-Gebühr'])
