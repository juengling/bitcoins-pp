'''
Created on 03.04.2020

@author: Chris
'''

# Command line exit codes
EXIT_OK = 0
EXIT_ERROR = 1

verbose = False  # pylint: disable=invalid-name
debug = False  # pylint: disable=invalid-name
