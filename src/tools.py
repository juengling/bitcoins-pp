'''
Created on 03.04.2020

@author: Chris
'''

from __init__ import __appname__, __hash__, __version__, __betaversion__


def name_version():
    '''
    Return program name and version

    :returns: Program name, version, and hash
    :rtype: str
    '''
    if __betaversion__:
        return '{} v{}-beta ({})'.format(__appname__, __version__, __hash__)
    else:
        return '{} v{} ({})'.format(__appname__, __version__, __hash__)
