'''
Created on 03.04.2020

@author: Chris
'''

import sys
from tools import name_version
from __init__ import __appname__, __desc__
from argparse import ArgumentParser
import public
from BitcoinsDe import BitcoinsDe
import logging


def main(argv):
    '''
    Main program

    :param argv: Command line arguments
    :type argv: list[str]
    :returns: Return code
    :rtype: int
    '''

    args = parse_command_line(argv)
    logger = logging.getLogger(__appname__)

    if args.version:
        print(name_version())
        return public.EXIT_OK

    try:
        bitcoin = BitcoinsDe(args.bitcoinfile)
    except FileNotFoundError:
        logger.error('File not found: %s', args.bitcoinfile)

    return 0


def parse_command_line(arguments):
    '''
    Parse command line arguments

    :param arguments: List of command line arguments
    :type arguments: list[str]
    '''
    parser = ArgumentParser(
        prog=__appname__, description=name_version() + ', ' + __desc__)

    parser.add_argument('-b', '--bitcoin', dest='bitcoinfile',
                        help='CSV file from bitcoin.de, may contain more than just BTC values')
    parser.add_argument('--version', action='store_true',
                        help='Print name and version, then exit.')

    args = parser.parse_args(arguments)

    return args


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
