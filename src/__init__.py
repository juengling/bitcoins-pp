# Program properties
__appname__ = 'Bitcoin-PP'
__desc__ = 'Programm zur Konvertierung der Kontoauszüge von Bitcoins.de und Fidor.de in ein für Portfolio Performance passendes Format'
__version__ = '0.1.0'
__betaversion__ = True
__hash__ = 'DEADC0DE'
